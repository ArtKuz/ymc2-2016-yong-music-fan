'use strict';

angular
  .module('YoungMusicFan', [
    'ionic'
  ])
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })
  .controller('RadioCtrl', function ($scope,
                                     $ionicLoading,
                                     $ionicPopup) {
    var vm = this,
      audioUrl = "http://mp3.metroaudio1.stream.avstreaming.net:7200/metro",
      options = {
        bgColor: "#FFFFFF",
        initFullscreen: false,
        errorCallback: function(errMsg) {
          $ionicPopup.alert({
            title: 'Error!',
            template: errMsg
          });
          $scope.state.play = false;
          $scope.state.pause = false;
        }
      };

    $scope.state = {
      play: false,
      pause: false
    };

    document.addEventListener("deviceready", function() {
      $scope.play = function() {
        if (!!window.Connection && (navigator.connection.type !== Connection.NONE && navigator.connection.type !== Connection.UNKNOWN)) {
          $ionicLoading.show({
            template: 'Loading...',
            duration: 1500
          });
          $scope.state.play = true;
          return window.plugins.streamingMedia.playAudio(audioUrl, options);
        } else {
          $ionicPopup.alert({
            title: 'Error!',
            template: 'No Internet connection'
          });
        }
      };

      $scope.stop = function() {
        $scope.state.play = false;
        return window.plugins.streamingMedia.stopAudio();
      };

      $scope.pause = function() {
        $scope.state.pause = true;
        return window.plugins.streamingMedia.pauseAudio();
      };

      $scope.resume = function() {
        $scope.state.pause = false;
        return window.plugins.streamingMedia.resumeAudio();
      };
    }, false);

  });
